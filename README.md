# RAW Notes (unedited)
Below are my raw notes that I used for LFCS (including grammar and spelling mistakes). Goodluck!

## Operations Deployment 25%

### Configure kernel parameters, persistent and non-persistent

- **Sysctl (runtime params)**
  - **sysctl** \- configure kernel parameters at runtime.
  - **sysctl** is used to modify kernel parameters at runtime. The parameters available are those listed under **/proc/sys/**. **Procfs** is required for **sysctl** support in Linux. You can use **sysctl** to both read and write **sysctl** data.
  - The **/proc/sys/** directory is different from others in **/proc/** because it not only provides information about the system but also allows the system administrator to immediately enable and disable kernel features.
  - a partial listing of **/proc/sys/fs**, if **w** can be used to configure the kernel.
    - To Read a Param: _sysctl net.ipv6.conf.default.disable_ipv6_
    - _net.ipv6.conf.default.disable_ipv6 = 0; where 0 = false, 1 = true_
    - **_sysctl -w net.ipv6.conf.default.disable_ipv6=1_** _(-w not sure)_
      - This is a **non-persistent** change to the parameter.
    - Also: **_echo 24000 > /proc/sys/kernel/pid_max_** (as root)
  - For **persistent** changes: **/etc/sysctl.d/\*.conf** files.
    - _vi /etc/sysctl.d/swapless.conf_
    - Loads on next boot or you can reload with:
      - _sysctl -p /etc/sysctl.d/swapless.conf_
  - Vendors put there settings in: **_/usr/lib/sysctl.d/_**
- **Kernel Boot Parameters & CMD Line**
  - The Kernel is responsible for lots of things.
  - See **man bootparam** - just doco (7)
  - _linux /boot/_**_vmlinuz-5.19.0_** _root=/dev/sda5 ro crashkernel=512M quiet selinux=0_
  - Boot options after **vmlinuz**, options not understood are passed to the first user process to be run.
  - Place in the Grub Config File, eg. _/boot/efi/EFI/centos/grub.cfg_
    - _Rocky:_ **_/boot/grub2/grubenv_**
  - See what command linux system was booted with: **_cat /proc/cmdline_**
- **Boot Process Failure**
  - No Bootloader Screen: re-install bootloader, corrupt boot sector.
  - Kernel fails to load: misconfig, corrupt kernel, incorrect boot params. Enter the GRUB menu to fix or use rescue image.
  - Kernel loads but fails to mount the root filesystem, misconfig, **grub.conf**, **/etc/fstab** or not support for filesystem or support in **initramfs** image.
  - Can try to boot into lower runlevel - **3(no graphics) 1(single user mode)**
- **Kernel Modules**
  - can be dynamically loaded and unloaded as needed after the system starts.
  - kernel modules are located in **_/lib/modules/$(uname -r)_** and can be compiled for specific kernel versions.
  - **lsmod**: List loaded modules.
  - **insmod**: Directly load modules.
    - Requires a fully qualified module name
    - **_insmod /lib/modules/$(uname -r)/kernel/drivers/net/ethernet/intel/e1000e.ko_**
  - **rmmod**: Directly remove modules.
  - **modprobe**: Load or unload modules, using a pre-built module database with dependency and location information:
    - **modprobe e1000e** and **modprobe -r e1000e**
    - **depmon** \- to generate or update file /lib/modules/$(uname -r)/modules.dep
  - **depmod**: Rebuild the module dependency database.
  - **modinfo**: Display information about a module.
    - **modinfo e1000e**
    - Can check /sys pseudo-filesystem
      - ls /sys/module/sg/parameters/
    - Modules can be loaded
      - sudo /sbin/insmod &lt;pathto&gt;/e1000e.ko debug=2 copybreak=256
    - If already loaded:
      - sudo /sbin/modprobe e1000e debug=2 copybreak=256
- **/etc/modprobe.d**
  - subdirectory tree which end with the .conf extension are scanned when modules are loaded and unloaded using modprobe.
    - Including module name aliases, automatically supplied options.
    - Can blacklist modules to avoid them being used.
  - they are loaded or unloaded, and configurations can be changed as needs change.
- **Kernel Modules Notes:**
  - - Modules loaded with non-acceptable open source licenses mark the kernel as tainted.
      - It is impossible to unload a module being used by one or more other modules
      - It is impossible to unload a module that is being used by one or more processes
      - When a module is loaded with modprobe, the system will automatically load any other modules that need to be loaded first.
      - When a module is unloaded with modprobe -r, the system will automatically unload any other modules being used by the module

### Diagnose, identify, manage and troubleshoot processes and services

- **ps**:2 kinds syntax styles
  - With - unix style eg. ps -a
  - With no - bsd style eg. ps a
- Examples ps:
  - ps aux
  - ps id - eg. ps 3434, **ps u 3434**
  - By User: ps -U dave, **ps u -U dave**
- **top**:
- **pgrep**: explore processes that have a specific name
  - **gprep -a syslog**
- **nice**: for scheduling priority
  - At launch of command: **nice -n 11 bash**
  - Process inherit nice value of ones that run them.
    - **ps lax**, or just **ps l - gives the nice value**
- **nice -n 12 bash** to setup nice value
- **renice -n 12** **existPID** \- to change nice value
- **ps -fas -** shows tree like branches for the process.
- Signals: **sigstop** and **sigkill** \- processors can’t ignore these
  - See list: **kill -L**
  - **kill -SIGHUP 12637** (can use numbers)
- Too named process:
  - Get process: **pgrep -a bash**
  - Send kill signal to named: **pkill -KILL bash**
- Process: **ctrl-z** - paused, **fg,** or **bg** \- or **cmd** with &
  - **jobs**\- to see what’s in thebackground
  - Return background job to foreground, **fg pid**
- See what processes have files open
  - **lsof -p pid**
  - Or what processes are using a file
  - **lsof /usr/bin/sleep**

### Manage or schedule jobs for executing commands

- **cron** (min, hours, days etc)
  - See **cat /etc/crontab**
  - \* (all) ,(multuple) -(range) /(specific steps)
  - **crontab -e** :edits users crontab
  - **crontab -l** :see crontab user, or **crontab -l -u dave**
  - **crontab -r** :to remove
  - Also directories**, /etc/cron.daily**, hourly, monthly, weekly
    - What ever finds in these directories it will run
    - No extension of scripts, and +rx
- **anacron** (simplest days - cron can miss,)
  - Syntax in: **vim /etc/anacrontab**
  - Verify syntax: **anacron -T**
  - **anacron -n -f** :will run **now** and **force** even if already run
- **at** (task only run once)
  - **at 15:00** : then prompt -enter commands then **ctl-d** to save job
  - **at ‘August 20 2022’**
  - **at ‘2:30 August 20 2022’**
  - **at ‘now + 3 hours’**
  - **atq:** list jobs
  - **at -c 20** :see move details
  - **atrm 20** :remove jobs
- Verify Jobs: **/var/log/cron**
  - Verify with grep cron, anacron and atd (at daemon)
  - MAILTO in /etc/crontab - local mail box - eg. /var/spool
  - NOTE: this is **COOL**: **systemd-cat --identifier="DMGW"**
  - **jourcalctl** :for all your commands

### Search for, install, validate, and maintain software packages or repositories

- dnf
  - dnf config-manager --add-repo <http://example.com/some/additional.repo>
  - dnf repoquery nginx --list
- Managing Software
  - apt: **apt update && apt install nginx**
  - **dpkg –listfiles** **nginx** :low level tool,
  - **dpkg –search** **/usr/sbin/nginx**
  - **apt show libnginx-mod-stream**
  - **apt search nginx**
  - **apt search --names-only nginx**
  - **apt remove nginx** :not dependencis
    - **apt autoremove** :or
    - **apt autoremove nginx**
- **Config Repos**
  - List**: add-apt-repository --list**
  - **/etc/apt/sources.list** and **sources.d** for 3rd party
  - Download repo key.
    - curl “http..gpg” -o docker.key
    - gpg –dearmor docker.key :outputs docker.key.gpg
    - mv docker.eky.gpg /etc/apt/keyrings
      - Need to then ref in sources deb \[signed-by=/etc/apt/keyrings\]
    - apt update
  - **PPA**: Personal Package Archive
    - **add-apt-repository ppa:graphics-drivers/ppa**
- **Install from Source**
  - Gave htop as an example, but basically follow the **README**

### Recover from hardware, operating system, or file system failures

- Sure

### Manage Virtual Machines (libvirt)

- Install: libvirt, virt-install, virt-manager, qemu-img, qemu-info
- Check the OS database: **osinfo-query os**
  - virsh undefine –remove-all-storage TestMachine
  - virsh autostart TestMachine, virsh autostart --disable TestMachine
  - virsh dominfoTestMachine
  - virsh setvcpus TechMachine 2 --config –maximum
    - virsh setvcpus TechMachine 2 --config
  - virsh setmaxmem TestMachine --size 2048MiB
  - virsh setmem --domain TestMachine --config --size 2048MiB
  - **Undefine** for removing and **Destroy** for stopping.
  - virsh console TestMachine
- **virt-install**
  - virt-install --import --memory 512 --disk /var/lib/libvirt/images/ubuntu-22.04-minimal-cloudimg-amd64.img --os-variant ubuntu22.04 --name dave --graphics none --vcpus 1 --cloud-init root-password-generate=yes or user-data=user-data
- qemu-img resize ubuntu-22.04-minimal-cloudimg-amd64.img 10G

### Configure container engines, create and manage containers

- docker search nginx
- docker rmi ubuntu/nginx - for image, docker rm dnginx - container
- docker run --d --publish 8080:80 --name nginx
  - 8080 is on the host, 80 on container
  - Check nginx with: **nc localhost 8080** \- then your in a terminal eg. GET /
- Restart: docker run -d --publish 8081:80 --restart always --name jnginx nginx
- Building:
  - FROM nginx
  - COPY index.html /usr/share/nginx/html/index.html
    - docker build --tag dave/nginxcust:1.0 .

### Create and enforce MAC using SELinux

- Installed via: selinux-basics, setools-console, auditd - allows to monitor
  - **sestatus** or **getenforce**
    - To set enforce: setenforce 1
    - For permanent: /etc/selinux/config
  - **ls -Z** - see MAC for **selinux**.
  - To enabled; **selinx-activate**
  - **cat /etc/default/grub** \- check for
- Using auditd
  - audit2why --all
  - ps -eZ - for selinux processes
  - audit2allow --all -M newmodave
    - Creates to files: .pp and .te
    - To load: semodule -i newmodave.pp
- system_u:system_r:init_t:s0 1
  - **user, role, type**
- chcon -u unconfined_u /var/log/auth.log
- chcon -r object_r /var/log/auth.log
- **See SELinux users: seinfo -u o -r or -t**
  - Need doco to find out what they mean.
- Use existing file to copy SELinux permissions
  - chcon --reference=/var/log/syslog /var/log/auth.log
- Can restore in directory: only restores correctly type label
  - **restorecon -R** /var/www
- **restorecon -R -F** /var/www - does users as well
- These can be lost, if relabeled:
- Persist with:
  - semanage fcontext --add --type var_log_t /var/www/10
  - Restorecon /var/www/10 - uses above type now
- List Context: semanage fcontext --list
- Then set context on new directly from one listed above:
  - semanage fcontext --add --type nfs_t “/nfs/shares(/.\*)?”
  - restorecon -R /nfs
- Booleans:
  - semanage boolean --list
  - setsebool virt_use_nfs 1
  - getsebool virt_use_nfs
  - semanage port --list
    - Add: semanage port --add --type ssh_port_t --proto tcp 2222
    - semanage delete --add --type ssh_port_t --proto tcp 2222

## Networking 25%

### Configure IPv4 and IPv6 networking and hostname resolution

- CIDR - classless inter-domain routing - 192.168.1.101/24 (32 bits)
  - /24 means 24 bits are zero aka subnet
    - 0000 0000 0000 0000 0000 0000 1111 1111
- IPv6 - 128 bits
  - **2001:0bd8:0000:0000:0000:ff00:0042:9341** \- each hex digit is 4 bits
  - Can be shorten by removing all leading 0s: **2001:bd8::ff00:42:9341**
  - CIDR: **2001:0bd8:0000:0000:0000:ff00:0042:9341/64 -** same as above
- ip -c a
- ip link set dev virbr0 up
- ip address add 192.168.1.143/24 dev enp0s3
- Ubuntu uses **netplan - /etc/netplan -yaml**
  - **netplan apply**
  - **netplan try**
  - **man netplan**
  - **ADDITIONAL DOCO: /usr/share/doc**
  - Add config file with 99:
    - **nameservers**:
    - address:
    - \- 8.8.8.8
  - And **routes**:
    - \- to: 192.168.0.0/24
    - via: 10.0.0.100
    - \- to: default
    - via: 10.0.0.1
- See changes: with ip a
- If resolving: resolvectl status (shortedL resovlectl dns)
  - **systemctl start systemd-resolved.service**
  - To change: /etc/systemd/resolved.conf
- /etc/hosts - I know this one!
- **Add Device:**
  - **ip link add name ubuntub type bridge**

### Set and synchronize system time using time servers

- chrony vs status systemd-timesyncd.service
- timedatectl timesync-status
  - ls -lh **/etc/systemd/system**/systemd-timedated.service

### Monitor and troubleshoot networking

- **ss** or netstat
- **ss** \-ltunp: l - listening, t - tcp , u - udp, n -numeric values, -p - process
  - \[::1\] - IPV6 like 0.0.0.0 - all
  - \[::\] - IPV6 like 127.0.0.1 - only localhost
- lsof -p pid 0 shows files open

### Configure the OpenSSH server and client

- Config for ssh: /etc/ssh
  - man sshd_config
  - man ssh_config
- SSH Server Settings: Port, AddressFamily, ListenAddress, PasswordAuthentication, PasswordAuthentication, X11Forwarding
- SSH Client: can create own in ~/.ssh/config Needs to be 600
  - None by default
  - man ssh_config
  - Host centos
    - Hostname: 192.168.1.251
    - Port: 22
    - User: aaron
- User ssh keys
  - ssh-keygen
  - ssh-copy-id dave@192.168.1.142
  - or: copy pub key directly into: vi ~/.ssh/authorized_keys
- Known Hosts: Can remove
  - ssh-keygen -R 192.168.1.142

### Configure packet filtering, port redirection, and NAT

- **Configure Packet Filtering**
  - **man ufw-framework**
  - ufw - ubuntu
  - ufw status
  - ufw status verbose
  - ufw enable
  - ufw allow 22/tcp
  - Active tcp connections
    - ss -tn
  - Rule
    - ufw allow from 192.168.1.60 to any port 22
  - Ordered rules:
    - ufw status numbered
    - Can delete: **ufw delete 1** or **ufw delete 22**
    - Insert at specific number: ufw insert 3 deny from 10.11.12.100
  - Rule
    - ufw allow **from** 192.138.11.0/24 to any port 22
    - ufw allow **from** 192.138.11.0/24
  - Rule
    - ufw deny **out** on enp1s0 to 8.8.8.8
  - Complex Rule
    - ufw allow in on enp0s3 from 192.168.1.60 to 192.168.1.81 port 80 proto tcp
    - ufw allow out on enp0s3 from 192.168.1.81 to 192.168.1.60 port 80 proto tcp
- Port redirection and NAT
  - Enabled Port Redirection: /etc/sysctl.d/99-sysctl.conf
  - man -s 5 sysctl.conf
  - Forward traffic
    - #net.ipv4.ip_forward=1
    - #net.ipv6.conf.all.forwarding=1
  - Load: sysctl --system
  - Check: sysctl -a
  - Netfilter Framework (nft): Kernel - so we use iptables
- Example:
  - iptables -t nat -A **PREROUTING** \-i enp1s0 -s 10.0.0.0/24 -p tcp --dport 8080 -j DNAT --to-destination 1
  - Applies to packets received from the outside world.
  - itables -t nat -A POSTROUTING -s 10.0.0.0/24 -o enp6so -j MASQUERADE
- **See Rules:** nft list ruleset
- To persist Rules: need: apt install iptables-persistent
  - then to save: netfilter-persistent save
- List current rules
  - iptables --list-rules --table nat
- Flush current rules:
  - iptables --flush --table nat

### [Configure static routing](https://jfearn.fedorapeople.org/fdocs/en-US/Fedora/20/html/Networking_Guide/sec-Connecting_to_a_Network_Using_nmcli.html)

- ip route add 192.168.122.52 via 192.168.1.142
- ip route add 192.168.122.52 via 192.168.1.142 dev enp0s3
- ip route add default via 10.0.0.100 - GATEWAY
- Not Persistence, so use: **nmcli con show**
- **nmcli con modify enp0s3 +ipv4.routes “192.168.0.0/24 10.0.0.100**”
  - **nmcli device reapply enp0s3**
  - nmcli dev status
  - nmcli con show
  - **nmcli con modify ethernet-dave ifname newdev**
    - Adds connection with device
  - ip route show
  - Also: **ip link add name newdev type bridge**

### [Configure Bridge and Bonding devices](https://developers.redhat.com/blog/2018/10/22/introduction-to-linux-interfaces-for-virtual-networking#macvlan)

- **Bridge** \- bridge between two or more networks
  - netplan example: /usr/share/doc/netplan/examples/
  - Example: with enp0s8 and enp0s9
    - ip link add name br17 type bridge
    - nmcli con add type bridge ifname br17
    - nmcli con modify bridge-br17 ifname br17
    - Set IP on br17 - connection comes up
    - ip link set enp0s8 master br17
    - ip link set enp0s9 master br17
- **Bond** \- take two or more network devices to and join them together so it’s seen as one.
  - changes made with ip are temp
  - again netplan - bu tried **netplan**
  - /usr/share/doc/iputils/README.bonding
  - Bonding modes, 0 to 6
    - Mode 0: Round-robin
    - Mode 1: active backup (uses only one)
    - Mode 2: XOR -go through same interface
    - Mode 3: Broadcast - all interfaces at once.
    - Mode 4: IEEE - increase speeds over single interface
    - Mode 5: Load Balance traffic.
  - All network interfaces are called “Ports”
  - Example: - delete bridge form above ip link delete br17
    - ip link add bond1 type bond miimon 100 mode active-backup
    - ip link set enp0s8 master bond1
    - ip link set enp0s9 master bond1

### seImplement reverse proxies and load balances

- nginx -t
- Reverse proxies: nginx
  - proxy_pass htp://1.1.1.1;
  - include proxy_parms;
    - /etc/nginx/proxy_params
  - server {  
        listen 80;  
        location /images {  
        proxy_pass http:1.1.1.1;  
        include proxy_params;  
        }  
        }
- Load balancer
  - upstream mywebservers {  
        laest_connl  
        server 1.2.3.4 weight=3 down;  
        server 5.6.7.8:8081;  
        Server 10.20.30.40 backup;  
        }  
        server {  
        listen 80;  
        location / {  
        proxy_pass <http://mywebservers>;  
        }  
        }

## Storage 20%

### Configure and manage LVM storage

- List Block Devices: lsblk
- man **lvm**
- Location Device: /dev/sda1 - sda - entire device, sda1 the partition.
- List partition on this block device: sda
  - fdisk --list /dev/sda
  - cfdisk /dev/sda
  - When listing, see type, as it can be **part** of **lvm**
- gpt - for mbr
- LVM - multiple disks as one partition: need lvm2
- **pv: physical volume**
  - real storage devices or partition
  - **lvmdiskscan** \- list devices that may be used as physical volumes
  - **pvcreate** /dev/sdb /dev/sdc
  - **pvs** :to see the physical volumes
- **vg: volume group**
  - vgcreate my_volume /dev/sdb /dev/sdc
  - vgs :to see the volume groups - allows to keep growing over time
  - pvcreate /dev/sdd
  - vgextend my_volume /dev/sdd
    - vgreduce my_volume /dev/sdd
      - pvremove /dev/sdd
- **lv: logical volume**
  - now to partition volume group
  - lvcreate --size 2G --name p1 my_volume
  - lvcreate --size 3.9G --name p2 my_volume
    - lvremove my_volume/p2
  - lvcreate --size 2G --name p2 my_volume
  - lvresize --extents 100%VG my_volume/p2
  - lvresize --size 2G my_volume/p2
- Now to make filesystem: **lvdisplay** - see the info about an lv - to use making filesyste
  - mkfs.xfs /dev/my_volume/p1
    - lvresize would only resize partition not filesystem
    - lvresize --**resizefs** --size 3G my_volume/p1
- pe: physical extent

### Manage and configure the virtual file system

- Inodes, ln hard links and software links ln -s
- **Supported file systems: cat /proc/filesytems**
- /proc Virtual file system representing different processes
  - Pseudofilesystem access to many kernel structures and subsystems
- /sys Virtual file system, similar to /proc Used as a device tree
- tmpfs - ram
  - mkdir /mnt/tmpfs
  - mount -t tmpfs none /mnt/tmpfs :: alots an size
    - \-o size=1G to change
  - df -h /mnt/tmpfs
  - used at **/dev/shm**
- Simplist way to make file system on partition.
  - mkfs.xfs /dev/sdb1, mkfs.ext4 /dev/sdb1
  - mkfs.xfs -i size-512 -L “BackUpVolume” /dev/sdb1
- xfs tab twice for options specific to xfs
  - xfs_admin
- ext4:
  - tune2fs for changing options about it.
- Mounting:
  - mount /dev/my_volume/p1 /mnt/
  - mkfs.ext4 /dev/my_volume/p2
- /etc/fstab - onboot up
  - blockdev, /mountpoint filesystem(xfs) default(mount options) 0(dump) 0,1,2(scan for errors), 0 for swap, 1 for root and 2 for others
  - reload with: systemctl daemon-reload
- Can also use something like UUID=22c9989d-12a3-430a-bdc5-96fbb7b2aa65 in lu of /dev/sda.
  - check id with: **blkid** /dev/sda1
- ls -l /dev - disk device nodes

### Create manage and troubleshoot filesystems

- findmnt - finds everything mounted.
- findmnt -t xfs,ext4
- mount -o ro,noexec,nosuid /dev/vdb2 /mnt
  - noexec - can’t run a program
  - nosuid - disable suid so root program can’t run with sudo
- Only mount once, but can remount - only for not specific file system usage, use umount then
  - **mount -o remount,rw,noexec,nosuid /dev/vdb2 /mnt**
- Mount options apply to all filesystems: man mount
  - some to filesystem: man xfs - has a mount option section
  - mount -o allocsize=32k /dev/vdb1 /mybackups
  - mount -a :mount all in fstab
- Manual above, otherwise we use **/etc/fstab**
  - /dev/vdb1 /mybackup xfs defaults 0 2
  - /dev/vdb1 /mybackup xfs ro,noexec 0 2
- Backup mbr
  - dd if=/dev/sda of=mbrbackup bs=512 count1
    - dd if=mrbackup of=/dev/sda bs512 count=1
- GPT System
  - **sgdisk** \-p /dev/sda
- Trouable Shooting
  - fdisk and sfdisk and counterpart for GPT: gdisk and sgdisk
  - Partitions OS currently away of: cat /proc/partitions
  - FileSystem - Attributes: lsattr and chattr
  - rpm -V some_package or rpm -Va for verifying all
  - Remount Root if RO - mount -o remount,rw /
    - if **fstab** correct, run **fsck**
  - **df: df -i(inodes), df -T(type),**
  - **du: du -h, du -a(files as well)**
  - **fuser -u index.html (although didn’t work)**
  - **lsof - probably need to get better.**

### Use remote filesystems and network block devices

- Mount File System on Demand, **autofs** is the most command
  - **nfs** for networked file systems. (dnf nfs-utils, apr nfs-kernel-server) - aufo
    - systemctl status nfs-server
  - On **Server**: vi /etc/exports << /etc 192.168.1.184(ro)
    - /src/home 192.168.10.32/24(rw), or \*(rw) any client
    - man exports - for options
    - exportfs -r :refresh
    - exportfs -v : version shows what’s being expotred
    - MOUNT SIMPLY with: mount 192.168.10.10:/path/remote /path/local
    - Can put that in fstab
  - On **Client**: vi /etc/auto.master << /shares/ /etc/auto.shares --timeout=400
    - **automount** \- manage autofs mount points
    - Above is relative: /- /etc/auto.shares --timeout=400
    - mount -t nfs server:/dir /mnt/dir
  - vi /etc/auto.shares << davesnetworkshare -fstype=auto,ro 192.168.1.142:/etc Above is relative: /dave -fstype=auto 192.168.1.142:/tmp
    - autofs - local fs: localshare -fstype=auto,ro :/etc
  - To use: ls **/shares/davesnetworkshare**
    - Debug commands
      - showmount -e 192.168.1.142
      - rpcinfo -p 192.168.1.142
  - for ext sytems
    - dump - backing up file systems and restoring.
    - dumpe2fs
- Network Block Device
  - nbd-server
  - /etc/nbd-server/config
    - man -s 5 nbd-server
  - **nbd-client -l localhost**
  - nbd-client
    - modpobe mbd
    - nbd-client address -N partition
      - give connection connected /dev/nbd0 then mount as usual
    - then detach with: nbd-client -d /dev/nbd0

### Configure and manage swap space

- Move form RAM to Disk
- **swapon --show**
- lsblk - shows what is swap
- **mkswap /dev/vbd3**
- swapon --verbose **/dev/vbd3**
- need to fstab or
- To stop using it at swap
  - **swapoff /dev/vdb3**
- Can also use simple files as swap: need to create a file of 0s
  - dd if=/dev/zero of=/swap bs=1M count=128
    - dd if=/dev/zero of=/swap bs=1M count=128 status=progress
    - only root should read this 600
  - mkswap /swap

### Configure filesystem automounters

- autofs as above.
- fstasb automount
  - LABEL=samusb /SAM ext4 noauto,x-systemd.automount,x-systemd.automount.device-timeout=10,x-systemd.automount.idle-timeout=30
    - **noauto**: do not mount at autoboot
    - **x-systemd.automount:** use systemd automount
    - **x-systemd.automount.device-timeout=10:** timeout after 10 sec
    - **x-systemd.automount.idle-timeout=30**: not use 30se, unmount

### Monitor storage performance

- Commands: read and write ops - installed via **sysstat**
- **iostat**, and **pidstat**
- **iostat:** historical data from since bootup
  - iostat 10 - last 10 seconds
  - iostat -d - move cpu states
  - iostat -h - human readable
  - iostat -p ALL
  - iostat -p vda
- **pidstat**:
  - pidstat -d - storage related
  - pidstat -d pid
  - pidstat -d --human

## Essential Commands 20%

### Basic Git Operations

- git config--global user.name “dave”
- git config--global user.email “[dave@dave.com](mailto:dave@dave.com)”
- cat .git/config
- Staging and committing:
  - git status, git add - adds to staging area.
  - to unstage: **git reset file**
  - **git add “\*.html”**
  - git add “products/\*.html”
  - **git commit -m “committed mate”**
  - **git rm file.txt**
- Git Branches
  - General:
    - git branch 1.2
    - git checkout 1.2
    - git log
    - git show b7a3
    - git merge 1.2 (in master)
  - Remote:
    - git remote -v - shows remote
    - git remote add origin [git@github.blah](mailto:git@github.blah)
      - origin is an alias for the url above
    - ssh-keygen (for remote)
    - git push origin master
    - git pull origin master
    - git clone

### Create, Configure and troubleshoot services

- Create systemd services
  - man -s 5 systemd.service
    - echo "work oh" | systemd-cat -t dave -p 6
  - Used: ExecStart=echo "DMGW DAVe work oh"
    - /etc/systemd/system/dave.service
    - or put a script in: /.usr/local/bin/dave_service.sh
  - systemctl daemon-reload

### Monitor and troubleshoot system performance and services

- top, iostat, vmstate, htop, free, sar, ss, journalctl -xe

### Determine application and service specific constraints

- top, free, fh, iftop, ulimit, systemctl ps aux

### Troubleshoot disk space issues

- du, df
- lsof | grep deleted

Work with SSL certificatess

- Creation of x.509 certificates
  - req- sub command for CSRs
  - man openssl req
  - Self signed: openssl req -x509 -newkey rsa:2048 -keyout key.pem -out req.pem
- To read:
  - openssl x509 -in req.pem --text --noout

## Users and Groups 10%

### Create and manage local user and group accounts

- gpasswd - administer /etc/group and /etc/gshadow

### Manage Personal and system-wide environment profiles

- ok

### Configure user resource limits

- ok

### Configure and manage ACLs

- ok

### Configure the system to use LDAP user and group accounts

- cat /etc/passwd
- id john
- installing ldap serve with lxc and install libnss-ldapd
- cat /etc/nsswitch.conf
  - ldaps added to 3 lines in that file
- cat /etc/nslcd.conf
  - points to ldap server
- get all users: getent passwd
  - getent passwd --service ldap
  - getent group --service ldap
- PAM: to create user direct
  - pam-auth-udpate

Exam: Mock Exam 1 (kk) - 15/25 - 60mins

- sudo chmod +t /opt/sticky
- find file because relative path - keep that in mind
- use passwd -l and passwd -u instead of usermod
- Resource Limits:
  - /etc/security/limits.conf

Exam Mock Exam 2

- Not getting anacron to work correctly atm
- john ALL=(ALL) NOPASSWD: /sbin/shutdown
  - probably don’t understand format as much as I should
- firewall-cmd
  - firewall-cmd –list-all
  - firewall-cmd –add-port=80/tcp –permanent
  - restart it
- Email: vi /etc/aliases, bob: root, john: [john@example.com](mailto:john@example.com), new aliases
- HTTP not indexing: Options Indexes
- fstab Mount options: /dev/vdb1 /backups xfs defaults 0 0
  - need to remember what the last 2 numbers mean.
- RAID: **mdadm** –create /dev/md0 –level=1 –raid-devices=2 /dev/vdc /dev/vdd
- disk quota: **xfs-quota** -x -c ‘limit bsoft=100m bhard=500m bob’ /mydata/

Exam Mock Exam 3:

- find collection -name \*.txt | xargs mv -t /opt/textfiles/
- find collection/ -user adm | xargs cp -p -t /opt/admfiles/
- find collection/ -mmin -60 | xargs cp -a -t /opt/oldfiles/

Exam Mock Exam 4:

- sysctl –system - to reload from standard area
- KNnow how to remove a user and direclt
  - TODO

IPTABLES: man iptables-extensionsn  

- Port 5000 should be closed
  - iptables -I INPUT -p tcp --dport 5000 -j DROP
- Redirect all traffic on port 6000 to local port 6001
  - iptables -A PREROUTIUNG -t nat -i eth0 -p tcp --dport 6000 -j REDIRECT --to-port 6001
- Port 6002 should only be accessible from IP 192.168.10.80
  - iptables -A INPUT -p tcp --dport 6002 -s 192.168.10.80 -j ACCEPT
  - iptables -A INPUT -p tcp --dport 6002 -j DROP
- Block all outgoing traffic to IP 192.168.10.70 (server app-srv1)
  - sudo iptables -A OUTPUT -d 192.168.10.70 -j DROP